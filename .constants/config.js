const inDevMode = false;
const remoteDB = true;
const port = 8085;

const __urls = {
    localhost: 'http://localhost:' + 2050,
    remote: 'http://taskman.renter.co.ke:' + port,
    db: {
        localhost: 'localhost',
        remote: '188.166.37.51'
    }
}

module.exports = {

    reportsUrl: (function () {
        return __urls.report;
    })(),

    baseUrl: (function () {
        if (inDevMode) 
            return __urls.localhost;
        else 
            return __urls.remote;
        }
    )(),

    dbHost: (function () {
        if (inDevMode && !remoteDB) 
            return __urls.db.localhost;
        else 
            return __urls.db.remote;
        }
    )(),

    __urls: __urls

};
