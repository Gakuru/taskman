#!/bin/bash

# Database credentials
user="********"
password="*******"
host="********"
db_name="*******"

# Other options
backup_path="/home/dbdump"

# Set default file permissions
umask 777

# Dump database into SQL file
mysqldump --routines=true --user=$user --password=$password --host=$host $db_name > $backup_path/taskman_dump.sql

# Check if dump was successful
if [ "$?" -eq 0 ]
then
  /usr/bin/python /var/www/taskman/.devops/uploadDBToDropBox.py
fi

# Delete files older than 30 days
find $backup_path/* -mtime +30 -exec rm {} \;
