const express = require('express');
const app = express();

const ports = [9998];
const bodyParser = require('body-parser');

const shell = require('shelljs');

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,x-access-token');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.post('/pushed', (req, res) => {
require('simple-git')('/var/www/taskman')
     .exec(() => console.log('Starting pull...'))
     .pull((err, update) => {
        if(update && update.summary.changes) {
           require('child_process').exec('pm2 restart taskapiserver taskmanClientServer');
        }
     })
     .exec(() => console.log('pull done.'));
   // shell.cd('../');
   // shell.exec('/var/www/rct/.deploy/pull.sh');
    res.send('ok');
});

ports.forEach((port) => {
    app.listen(port, () => {});
});
