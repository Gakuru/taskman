import dropbox

class TransferData:
    def __init__(self, access_token):
        self.access_token = access_token

    def upload_file(self, file_from, file_to):
        """upload a file to Dropbox using API v2
        """
        dbx = dropbox.Dropbox(self.access_token)

        with open(file_from, 'rb') as f:
            dbx.files_upload(f.read(), file_to,mode=dropbox.files.WriteMode.overwrite)

def main():
    access_token = 'L0jDJk5Pw7QAAAAAAAAQUwwrsM97WOEusiwv1daJTSTURJrLk9n77MRq6OboWng7'
    transferData = TransferData(access_token)

    file_from = '/home/dbdump/taskman_dump.sql'
    file_to = '/taskman/dbdump/taskman_dump.sql'  # The full path to upload the file to, including the file name

    # API v2
    transferData.upload_file(file_from, file_to)

if __name__ == '__main__':
    main()
