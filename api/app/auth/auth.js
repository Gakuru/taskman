const errors = require('../utils/error');
const appUser = require('./user');

module.exports = {
    verify: (req, res, next) => {
        if (req.token) {
            appUser.user = req.token.user;
            next();
        } else {
            res.json(errors.respondToInvalidToken);
        }
    }
};