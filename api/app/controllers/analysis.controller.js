const Analysis = require('../models/analysis.model');

const COMMAND = require('../utils/crudCommands');

module.exports = {
    analyseTask: (args) => {
        const {id, callback} = args;
        Analysis
            .analyseTask(id)
            .then(result => {
                callback(result);
            })
            .catch(err => {
                callback(err);
            });
    }
};