const Employee = require('../models/employee.model');

const COMMAND = require('../utils/crudCommands');

module.exports = {
    get: (args) => {
        const {
            id,
            callback
        } = args;
        Employee
            .get(id)
            .then(result => {
                callback(result);
            })
            .catch(err => {
                callback(err);
            });
    },
    create: (args) => {
        const {
            data,
            callback
        } = args;
        Employee
            .save(data, COMMAND.SAVE)
            .then(result => {
                callback(result);
            })
            .catch(err => {
                callback(err);
            });
    },
    update: (args) => {
        const {
            data,
            callback
        } = args;
        Employee
            .save(data, COMMAND.UPDATE)
            .then(result => {
                callback(result);
            })
            .catch(err => {
                callback(err);
            });
    },
    destroy: (args) => {
        const {
            data,
            callback
        } = args;
        Employee
            .remove(data, COMMAND.TRASH)
            .then(result => {
                callback(result);
            })
            .catch(err => {
                callback(err);
            });
    }
};