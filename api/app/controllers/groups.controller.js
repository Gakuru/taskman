const Group = require('../models/groups.model');

const COMMAND = require('../utils/crudCommands');

module.exports = {
    get: (args) => {
        const {id, callback} = args;
        Group
            .get(id)
            .then(result => {
                callback(result);
            })
            .catch(err => {
                callback(err);
            });
    },
    find: (args) => {
        const {param, callback} = args;
        Group
            .find(param)
            .then(result => {
                callback(result);
            })
            .catch(err => {
                callback(err);
            });
    },
    create: (args) => {
        const {data, callback} = args;
        Group
            .save(data, COMMAND.SAVE)
            .then(result => {
                callback(result);
            })
            .catch(err => {
                callback(err);
            });
    },
    addMember: (args) => {
        const {data, callback} = args;
        Group
            .save(data, COMMAND.ADDMEMBER)
            .then(result => {
                callback(result);
            })
            .catch(err => {
                callback(err);
            });
    },
    update: (args) => {
        const {data, callback} = args;
        Group
            .save(data, COMMAND.UPDATE)
            .then(result => {
                callback(result);
            })
            .catch(err => {
                callback(err);
            });
    },
    destroy: (args) => {
        const {data, callback} = args;
        Group
            .remove(data, COMMAND.TRASH)
            .then(result => {
                callback(result);
            })
            .catch(err => {
                callback(err);
            });
    }
};