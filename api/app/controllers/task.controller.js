const Task = require('../models/task.model');

const COMMAND = require('../utils/crudCommands');

module.exports = {
    get: (args) => {
        const {id, callback} = args;
        Task
            .get(id)
            .then(result => {
                callback(result);
            })
            .catch(err => {
                callback(err);
            });
    },
    create: (args) => {
        const {data, callback} = args;
        Task
            .save(data, COMMAND.SAVE)
            .then(result => {
                callback(result);
            })
            .catch(err => {
                callback(err);
            });
    },
    tag: (args) => {
        const {data, callback} = args;
        Task
            .save(data, COMMAND.TAG)
            .then(result => {
                callback(result);
            })
            .catch(err => {
                callback(err);
            });
    },
    groupAssign: (args) => {
        const {data, callback} = args;
        Task
            .save(data, COMMAND.GROUPASSIGN)
            .then(result => {
                callback(result);
            })
            .catch(err => {
                callback(err);
            });
    },
    update: (args) => {
        const {data, callback} = args;
        Task
            .save(data, COMMAND.UPDATE)
            .then(result => {
                callback(result);
            })
            .catch(err => {
                callback(err);
            });
    },
    destroy: (args) => {
        const {data, callback} = args;
        Task
            .remove(data, COMMAND.TRASH)
            .then(result => {
                callback(result);
            })
            .catch(err => {
                callback(err);
            });
    }
};