const connection = require('../db/connect');

const appUser = require('../auth/user');

const Analysis = module.exports = {
    taskAnalysis: {
        keys: [
            'pending',
            'complete',
            'private',
            'public',
            'today',
            'this week'
        ]
    },
    analyseTask: () => {
        return new Promise((resolve, reject) => {
            let response = {
                status: 200,
                success: true,
                action: 'Get Task Analysis',
                message: 'Task Analysis retrieved successfuly',
                taskAnalysis: {}
            };

            connection.query('CALL `tasks_analysis` (?)', [appUser.user.id], (err, _results, fields) => {
                if (err) {
                    response.message = 'Task Analysis could not be retrieved';
                    return reject(response);
                } else {
                    const results = _results[0];

                    let status = {
                        pending: {
                            name: 'pending',
                            count: 5
                        }
                    };

                    let taskAnalysis = {};
                    let resultSetDimensions = [];

                    // Get all keys for the resultset and construct our analysis object
                    results.forEach((result) => {
                        resultSetDimensions.push(result.key);
                        taskAnalysis[result.key] = {};
                    });

                    results.forEach(result => {

                        if (resultSetDimensions.includes(result.key)) {
                            taskAnalysis[result.key][
                                result
                                    .discriminator
                                    .toLowerCase()
                                    .split(' ')
                                    .join('')
                            ] = {
                                name: result
                                    .discriminator
                                    .toLowerCase(),
                                count: result.count
                            };
                        }
                    });

                    response.taskAnalysis = taskAnalysis;

                    return resolve(response);
                }
            });
        });
    }
};