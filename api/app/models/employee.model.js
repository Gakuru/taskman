const connection = require('../db/connect');

const crudCommand = require('../utils/crudCommands');

const uuid = require('uuid/v1');

const buildQuery = (command) => {
    // let query = undefined;
    if (command === crudCommand.SAVE) {
        return 'INSERT INTO `employees` SET ?;';
    } else if (command === crudCommand.UPDATE) {
        return 'UPDATE `employees` SET ? WHERE ?;';
    }
};

const Employee = module.exports = {
    Employee: {
        id: undefined,
        firstName: undefined,
        lastName: undefined,
        phone: undefined,
        Email: undefined
    },
    get: (id) => {
        return new Promise((resolve, reject) => {
            let response = {
                status: 200,
                success: true,
                action: 'Get Employee',
                message: 'Employees retrieved successfuly',
                tasks: {}
            };
            return resolve(response);
            // if (id) {
            //     response.message = `Task '${id}' retrieved successfuly`;
            //     return resolve(response);
            // } else {
            //     return resolve(response);
            // }
        });
    },
    save: (data, command) => {
        return new Promise((resolve, reject) => {

            if (command === crudCommand.SAVE) {
                data.id = uuid();
            }

            connection.query(buildQuery(command), data, (err, results, fields) => {
                if (err)
                    return reject({
                        status: 400,
                        success: false,
                        action: 'Save Employee',
                        command: command,
                        message: 'Saving employee failed',
                        err: err
                    });
                return resolve({
                    status: 200,
                    success: true,
                    action: 'Save Employee',
                    command: command,
                    message: 'Employee saved successfuly',
                    employee: data
                });
            });
        });
    },
    remove: (data, command) => {
        return new Promise((resolve, reject) => {
            let a = true;
            if (a)
                return resolve({
                    status: 200,
                    success: true,
                    action: 'Remove Employee',
                    message: 'Employee removed successfuly'
                });
            else
                return reject({
                    status: 400,
                    success: false,
                    action: 'Remove Employee',
                    message: 'Employee removal failed'
                });
        });
    }
};