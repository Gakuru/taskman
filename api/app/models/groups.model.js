const connection = require('../db/connect');

const crudCommand = require('../utils/crudCommands');

const uuid = require('uuid/v1');

const appUser = require('../auth/user');

const buildQuery = (command) => {
    // let query = undefined;
    if (command === crudCommand.SAVE) {
        return 'INSERT INTO `groups` SET ?;';
    } else if (command === crudCommand.UPDATE) {
        return 'UPDATE `groups` SET ? WHERE ?;';
    } else if (command === crudCommand.ADDMEMBER) {
        return 'INSERT INTO `group_members` SET ?;';
    }
};

const Groups = module.exports = {
    Group: {
        id: undefined,
        creatorId: undefined,
        name: undefined
    },
    get: (id) => {
        return new Promise((resolve, reject) => {
            let response = {
                status: 200,
                success: true,
                action: 'Get Group',
                message: 'Groups retrieved successfuly',
                groups: {}
            };

            connection.query('CALL `get_groups` (?)', [appUser.user.id], (err, results, fields) => {
                if (err) {
                    response.message = 'Groups could not be retrieved';
                    return reject(response);
                } else {
                    response.groups = results[0];
                    return resolve(response);
                }
            });
        });
    },
    find: (param) => {
        return new Promise((resolve, reject) => {
            let response = {
                status: 200,
                success: true,
                action: 'Find Group',
                message: 'Users retrieved successfuly',
                groups: {}
            };

            connection.query('CALL `find_group`(?);', [param], (err, results, fields) => {
                if (err) {
                    response.message = 'Groups could not be retrieved';
                    return reject(response);
                } else {
                    response.groups = results[0];
                    return resolve(response);
                }
            });
        });
    },
    save: (data, command) => {
        return new Promise((resolve, reject) => {

            if (command === crudCommand.SAVE || crudCommand.ADDMEMBER) {
                data.id = uuid();
            }

            connection.query(buildQuery(command), data, (err, results, fields) => {
                if (err) 
                    return reject({
                        status: 400,
                        success: false,
                        action: 'Save Group',
                        command: command,
                        message: 'Saving group failed',
                        err: err
                    });
                return resolve({
                    status: 200,
                    success: true,
                    action: 'Save Group',
                    command: command,
                    message: 'Group saved successfuly',
                    group: data
                });
            });
        });
    },
    remove: (data) => {
        return new Promise((resolve, reject) => {
            connection.query('DELETE FROM `groups` WHERE ?;', {
                id: data.id
            }, (err, response, fields) => {
                if (err) {
                    return reject({status: 400, success: false, action: 'Remove Group', message: 'Group removal failed'});
                } else {
                    Groups
                        .get()
                        .then(response => {
                            return resolve({status: 200, success: true, action: 'Remove Group ', message: ' Group removed successfuly ', groups: response.groups});
                        });
                }
            });
        });
    }
};