const connection = require('../db/connect');

const crudCommand = require('../utils/crudCommands');

const uuid = require('uuid/v1');

const appUser = require('../auth/user');

const taskStatus = {
    PENDING: 'PENDING',
    COMPLETED: 'COMPLETED'
};

const buildQuery = (command) => ({
    [crudCommand.SAVE]: 'INSERT INTO tasks SET ?;',
    [crudCommand.UPDATE]: 'UPDATE `tasks` SET ? WHERE ?;',
    [crudCommand.TAG]: 'INSERT INTO `task_tags` SET ?;',
    [crudCommand.GROUPASSIGN]: 'INSERT INTO `group_tasks` SET ?;'
})[command];

const dataConfig = (data, command) => {
    return {
        [crudCommand.SAVE]: () => {
            data.id = uuid();
            data.status = taskStatus.PENDING;
            return data;
        },
        [crudCommand.UPDATE]: () => {
            return data;
        },
        [crudCommand.TAG]: () => {
            data.id = uuid();
            return data;
        },
        [crudCommand.GROUPASSIGN]: () => {
            data.id = uuid();
            return data;
        }
    }[command]();
};

function * queryConfig(data, command) {
    yield buildQuery(command);
    yield dataConfig(data, command);
}

const Task = module.exports = {
    Task: {
        id: undefined,
        title: undefined,
        description: undefined,
        creatorId: undefined,
        expectedCompletionDate: undefined
    },
    get: (id) => {
        return new Promise((resolve, reject) => {
            let response = {
                status: 200,
                success: true,
                action: 'Get Task',
                message: 'Tasks retrieved successfuly',
                tasks: {}
            };

            connection.query('CALL `get_tasks` (?)', [appUser.user.id], (err, results, fields) => {
                if (err) {
                    response.message = 'Tasks could not be retrieved';
                    return reject(response);
                } else {
                    response.tasks = results[0];
                    return resolve(response);
                }
            });
        });
    },
    save: (data, command) => {
        return new Promise((resolve, reject) => {

            let query = queryConfig(data, command);

            connection.query(query.next().value, query.next().value, (err, results, fields) => {
                if (err) 
                    return reject({
                        status: 400,
                        success: false,
                        action: 'Save Task',
                        command: command,
                        message: 'Saving task failed',
                        err: err
                    });
                return resolve({
                    status: 200,
                    success: true,
                    action: 'Save Task',
                    command: command,
                    message: 'Task saved successfuly',
                    task: data
                });
            });
        });
    },
    remove: (data) => {
        return new Promise((resolve, reject) => {
            connection.query('DELETE FROM `tasks` WHERE ?;', {
                id: data.id
            }, (err, response, fields) => {
                if (err) {
                    return reject({status: 400, success: false, action: 'Remove Task', message: 'Task removal failed'});
                } else {
                    Task
                        .get()
                        .then(response => {
                            return resolve({status: 200, success: true, action: 'Remove Task ', message: ' Task removed successfuly ', tasks: response.tasks});
                        });
                }
            });
        });
    }
};