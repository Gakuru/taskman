const connection = require('../db/connect');

const crudCommand = require('../utils/crudCommands');

const uuid = require('uuid/v1');

const jwt = require('jsonwebtoken');

const appUser = require('../auth/user');

const buildQuery = (command) => ({
    [crudCommand.SAVE]: 'CALL `insert_user` (?,?,?,?,?,?);'
})[command];

const dataConfig = (data, command) => {
    return {
        [crudCommand.SAVE]: () => {
            data.id = uuid();
            return [
                data.id,
                data.first_name,
                data.last_name,
                data.name,
                data.email,
                data.image_url
            ];
        }
    }[command]();
};

function * queryConfig(data, command) {
    yield buildQuery(command);
    yield dataConfig(data, command);
}

const User = module.exports = {
    User: {
        id: undefined,
        first_name: undefined,
        last_name: undefined,
        name: undefined,
        email: undefined,
        image_url: undefined
    },
    get: (id) => {
        return new Promise((resolve, reject) => {
            let response = {
                status: 200,
                success: true,
                action: 'Get User',
                message: 'Users retrieved successfuly',
                users: {}
            };

            connection.query('CALL `get_users`;', (err, results, fields) => {
                if (err) {
                    response.message = 'Users could not be retrieved';
                    return reject(response);
                } else {
                    response.users = results[0];
                    return resolve(response);
                }
            });
        });
    },
    find: (param) => {
        return new Promise((resolve, reject) => {
            let response = {
                status: 200,
                success: true,
                action: 'Find User',
                message: 'Users retrieved successfuly',
                users: {}
            };

            connection.query('CALL `find_user`(?);', [param], (err, results, fields) => {
                if (err) {
                    response.message = 'Users could not be retrieved';
                    return reject(response);
                } else {
                    response.users = results[0];
                    return resolve(response);
                }
            });
        });
    },
    save: (data, command) => {
        return new Promise((resolve, reject) => {

            let query = queryConfig(data, command);

            connection.query(query.next().value, query.next().value, (err, results, fields) => {
                if (err) 
                    return reject({
                        status: 400,
                        success: false,
                        action: 'Save User',
                        command: command,
                        message: 'Saving task failed',
                        err: err
                    });
                
                if (results.affectedRows === 0) {
                    connection.query('SELECT id from users where ?', {
                        email: data.email
                    }, (err, results, fields) => {
                        data.id = results[0].id;
                        let token = jwt.sign({
                            user: {
                                id: data.id,
                                email: data.email
                            },
                            exp: Math.floor(Date.now() / 1000) + ((60 * 1) * 60) //Expires in three hours
                        }, 'secret');

                        // return the information including token as JSON res.json();

                        return resolve({
                            status: 200,
                            success: true,
                            action: 'Auth User',
                            command: command,
                            message: 'User Oauthed successfuly',
                            user: {
                                user: data,
                                token: token
                            }
                        });
                    });
                } else {
                    let token = jwt.sign({
                        user: {
                            id: data.id,
                            email: data.email
                        },
                        exp: Math.floor(Date.now() / 1000) + ((60 * 1) * 60) //Expires in three hours
                    }, 'secret');

                    // return the information including token as JSON res.json();

                    return resolve({
                        status: 200,
                        success: true,
                        action: 'Auth User',
                        command: command,
                        message: 'User Oauthed successfuly',
                        user: {
                            user: data,
                            token: token
                        }
                    });
                }

            });
        });
    },
    remove: (data, command) => {
        return new Promise((resolve, reject) => {
            let a = true;
            if (a) {
                return resolve({status: 200, success: true, action: 'Remove User', message: 'User removed successfuly'});
            } else {
                return reject({status: 400, success: false, action: 'Remove User', message: 'User removal failed'});
            }
        });
    }
};