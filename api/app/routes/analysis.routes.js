const express = require('express');
const router = express.Router();

const common = require('../utils/common');

const Analysis = require('../controllers/analysis.controller');

router.get('/', (req, res) => {
    res.send(common.AppName);
});

router.get('/analysis/tasks', (req, res) => {
    Analysis.analyseTask({
        id: req.params.id,
        callback: (result) => {
            res.json(result);
        }
    });
});

module.exports = router;