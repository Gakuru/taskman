const express = require('express');
const router = express.Router();

const common = require('../utils/common');

const EmployeeCtrl = require('../controllers/employee.controller');

router.get('/', (req, res) => {
    res.send(common.AppName);
});

router.get('/employees/:id', (req, res) => {
    EmployeeCtrl.get({
        id: req.params.id,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/employees', (req, res) => {
    EmployeeCtrl.get({
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/employees/page/:page', (req, res) => {
    EmployeeCtrl.get({
        page: parseInt(req.params.page),
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/employees/:id', (req, res) => {
    EmployeeCtrl.get({
        id: req.params.id,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.post('/employee', (req, res) => {
    EmployeeCtrl.create({
        data: {
            first_name: req.body.firstName,
            last_name: req.body.lastName,
            mobile_no: req.body.mobileNo,
            email: req.body.email,
        },
        callback: (result) => {
            res.json(result);
        }
    });
});

router.patch('/employee', (req, res) => {
    EmployeeCtrl.update({
        data: [{
            first_name: req.body.firstName,
            last_name: req.body.lastName,
            mobile_no: req.body.mobileNo,
            email: req.body.email,
        }, {
            id: req.body.id
        }],
        callback: (result) => {
            res.json(result);
        }
    });
});

router.delete('/employee', (req, res) => {
    EmployeeCtrl.destroy({
        data: {
            id: req.body.id
        },
        callback: (result) => {
            res.json(result);
        }
    });
});

module.exports = router;