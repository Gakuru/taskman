const express = require('express');
const router = express.Router();

const common = require('../utils/common');

const GroupsCtrl = require('../controllers/groups.controller');

router.get('/', (req, res) => {
    res.send(common.AppName);
});

router.get('/groups/:id', (req, res) => {
    GroupsCtrl.get({
        id: req.params.id,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/groups', (req, res) => {
    GroupsCtrl.get({
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/groups/find/:param', (req, res) => {
    GroupsCtrl.find({
        param: req.params.param,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/groups/page/:page', (req, res) => {
    GroupsCtrl.get({
        page: parseInt(req.params.page),
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/groups/:id', (req, res) => {
    GroupsCtrl.get({
        id: req.params.id,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.post('/group', (req, res) => {
    GroupsCtrl.create({
        data: {
            creator_id: req.body.creatorId,
            name: req.body.name
        },
        callback: (result) => {
            res.json(result);
        }
    });
});

router.post('/group/member/add', (req, res) => {
    GroupsCtrl.addMember({
        data: {
            group_id: req.body.groupId,
            user_id: req.body.userId
        },
        callback: (result) => {
            res.json(result);
        }
    });
});

router.patch('/group', (req, res) => {
    GroupsCtrl.update({
        data: [
            {
                creator_id: req.body.creator_id,
                name: req.body.name
            }, {
                id: req.body.id
            }
        ],
        callback: (result) => {
            res.json(result);
        }
    });
});

router.delete('/group', (req, res) => {
    GroupsCtrl.destroy({
        data: {
            id: req.body.id
        },
        callback: (result) => {
            res.json(result);
        }
    });
});

module.exports = router;