const express = require('express');
const router = express.Router();

const common = require('../utils/common');

const TaskCtrl = require('../controllers/task.controller');

router.get('/', (req, res) => {
    res.send(common.AppName);
});

router.get('/tasks/:id', (req, res) => {
    TaskCtrl.get({
        id: req.params.id,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/tasks', (req, res) => {
    TaskCtrl.get({
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/tasks/page/:page', (req, res) => {
    TaskCtrl.get({
        page: parseInt(req.params.page),
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/tasks/:id', (req, res) => {
    TaskCtrl.get({
        id: req.params.id,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.post('/task', (req, res) => {
    TaskCtrl.create({
        data: {
            creator_id: req.body.creatorId,
            text: req.body.text,
            scope: req.body.scope
        },
        callback: (result) => {
            res.json(result);
        }
    });
});

router.post('/task/tag', (req, res) => {
    TaskCtrl.tag({
        data: {
            user_id: req.body.userId,
            task_id: req.body.taskId
        },
        callback: (result) => {
            res.json(result);
        }
    });
});

router.post('/task/group/assign', (req, res) => {
    TaskCtrl.groupAssign({
        data: {
            group_id: req.body.groupId,
            task_id: req.body.taskId
        },
        callback: (result) => {
            res.json(result);
        }
    });
});

router.patch('/task', (req, res) => {
    TaskCtrl.update({
        data: [
            {
                creator_id: req.body.creator_id,
                text: req.body.text,
                status: req.body.status,
                scope: req.body.scope
            }, {
                id: req.body.id
            }
        ],
        callback: (result) => {
            res.json(result);
        }
    });
});

router.delete('/task', (req, res) => {
    TaskCtrl.destroy({
        data: {
            id: req.body.id
        },
        callback: (result) => {
            res.json(result);
        }
    });
});

module.exports = router;