const express = require('express');
const router = express.Router();

const common = require('../utils/common');

const UserCtrl = require('../controllers/user.controller');

router.get('/', (req, res) => {
    res.send(common.AppName);
});

router.get('/users/:id', (req, res) => {
    UserCtrl.get({
        id: req.params.id,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/users', (req, res) => {
    UserCtrl.get({
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/users/find/:param', (req, res) => {
    UserCtrl.find({
        param: req.params.param,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/users/page/:page', (req, res) => {
    UserCtrl.get({
        page: parseInt(req.params.page),
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/users/:id', (req, res) => {
    UserCtrl.get({
        id: req.params.id,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.post('/user', (req, res) => {
    UserCtrl.create({
        data: {
            first_name: req.body.firstName,
            last_name: req.body.lastName,
            name: req.body.name,
            email: req.body.email,
            image_url: req.body.imageUrl
        },
        callback: (result) => {
            res.json(result);
        }
    });
});

router.post('/user/tag', (req, res) => {
    UserCtrl.tag({
        data: {
            employee_id: req.body.employeeId,
            task_id: req.body.taskId
        },
        callback: (result) => {
            res.json(result);
        }
    });
});

router.post('/user/group/assign', (req, res) => {
    UserCtrl.groupAssign({
        data: {
            group_id: req.body.groupId,
            task_id: req.body.taskId
        },
        callback: (result) => {
            res.json(result);
        }
    });
});

router.patch('/user', (req, res) => {
    UserCtrl.update({
        data: [
            {
                creator_id: req.body.creatorId,
                text: req.body.text,
                status: req.body.status
            }, {
                id: req.body.id
            }
        ],
        callback: (result) => {
            res.json(result);
        }
    });
});

router.delete('/user', (req, res) => {
    UserCtrl.destroy({
        data: {
            id: req.body.id
        },
        callback: (result) => {
            res.json(result);
        }
    });
});

module.exports = router;