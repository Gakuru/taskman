module.exports = {
    SAVE: 'SAVE',
    ADDMEMBER: 'ADDMEMBER',
    UPDATE: 'UPDATE',
    TRASH: 'TRASH',
    DELETE: 'DELETE',
    TAG: 'TAG',
    GROUPASSIGN: 'GROUPASSIGN'
};