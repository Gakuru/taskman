module.exports = {
    /**
     * TODO refactor for inheritance sake
     */
    throwSQLErr: (err) => {
        let e = undefined,
            _e = undefined,
            dupedKey = undefined;
        if (err.errno === 1062) {
            _e = err
                .sqlMessage
                .split('\'');
            switch (_e[3]) {
                case 'email':
                    e = `The email '${_e[1]}' already exists`;
                    break;
                case 'phone':
                    e = `The phone number '${_e[1]}' already exists`;
                    break;
                case 'service_per_provider':
                    e = 'The service is already assigned to the provider';
                    break;
                case 'name':
                    e = `The service name '${_e[1]}' already exists`;
                    break;
                case 'email_discriminator':
                    e = `The email address has already been registed as a client '${_e[1]}'`;
                    break;
                default:
                    e = err;
                    break;
            }
        }

        return {reason: err.code, message: e};
    },
    respondWith401: {
        code: 401,
        message: 'unauthorized request',
        fooMessage: 'Cheating Ha!'

    },
    respondWith404: {
        code: 404,
        message: 'No Data',
        fooMessage: 'Cheating Ha!'
    },
    respondWithCustomMessage: message => {
        return {code: 403, message: message, fooMessage: 'Aha, have you seen the custom message'};
    },
    respondToInvalidToken: {
        code: 403,
        message: 'Invalid token',
        unOAuthed: true,
        fooMessage: 'Got Yah! No way through'
    }
};