const express = require('express');
const app = express();

const ports = [2050, 2051];
const bodyParser = require('body-parser');

const jwt = require('jsonwebtoken');

const auth = require('../api/app/auth/auth');

// const auth = require('./auth/auth');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,x-access-token');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

const task = require('./app/routes/task.routes');
// const employee = require('./app/routes/employee.routes');
const group = require('./app/routes/groups.routes');
const analysis = require('./app/routes/analysis.routes');
const user = require('./app/routes/user.routes');

// const enrolment = require('./routes/enrolment'); const user =
// require('./routes/user');

app.use((req, res, next) => {
    let _token = req.headers['x-access-token'];

    if (_token) {
        jwt
            .verify(_token, 'secret', function (err, decode) {
                if (err) 
                    req.token = undefined;
                req.token = decode;
                next();
            });
    } else {
        req.token = undefined;
        next();
    }
});

app.use('/', user);
app.use('/', auth.verify, analysis);
app.use('/', auth.verify, task);
app.use('/', auth.verify, group);
// app.use('/', employee);

ports.forEach((port) => {
    app.listen(port, () => {
        console.log('api listening on port %s', port);
    });
});