const assert = require('assert');

const request = require('supertest');

const url = require('../../.constants/config');

describe('Test Employee Routes', () => {

    let employee = {
        id: undefined,
        firstName: undefined,
        lastName: undefined,
        mobileNo: undefined,
        email: undefined
    };

    it('It should pass if it gets emplyees [GET /employees]', (done) => {
        request(url.baseUrl)
            .get('/employees')
            .then(response => {
                assert(response.body.success, true);
                done();
            });
    });

    it('It should pass if it gets a single employee [GET /employees/123]', (done) => {
        request(url.baseUrl)
            .get('/employees/123')
            .then(response => {
                assert(response.body.success, true);
                done();
            });
    });

    it('It should pass if it posts an employee [POST /employee]', (done) => {
        request(url.baseUrl)
            .post('/employee')
            .send({
                firstName: 'John',
                lastName: 'Doe',
                mobileNo: '0720123456',
                email: 'johndoe@cytonn.com'
            })
            .expect(response => {
                employee.id = response.body.employee.id;
                assert(response.body.success, true);
            })
            .end((err) => {
                if (err)
                    return done(err);
                done();
            });
    });

    it('It should pass if it patches an employee [PATCH /employee]', (done) => {
        request(url.baseUrl)
            .patch('/employee')
            .send({
                id: employee.id,
                firstName: 'James',
                lastName: 'May',
                mobileNo: '0731123456',
                email: 'jmay@cytonn.com'
            })
            .expect(response => {
                assert(response.body.success, true);
            })
            .end((err) => {
                if (err)
                    return done(err);
                done();
            });
    });

    it('It should pass if it removes an employee [DELETE /employee]', (done) => {
        request(url.baseUrl)
            .delete('/employee')
            .send({
                id: 123
            })
            .expect(response => {
                assert(response.body.success, true);
            })
            .end((err) => {
                if (err)
                    return done(err);
                done();
            });
    });
});