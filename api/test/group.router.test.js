const assert = require('assert');

const request = require('supertest');

const url = require('../../.constants/config');

const uuid = require('uuid/v1');


describe('Test Groups Routes', () => {

    let group = {
        id: undefined,
        creatorId: undefined,
        name: undefined
    };

    it('It should pass if it gets groups [GET /groups]', (done) => {
        request(url.baseUrl).get('/groups').then(response => {
            assert(response.body.success, true);
            done();
        });
    });

    // it('It should pass if it gets group members [GET /groups]', (done) => {
    //     request(url.baseUrl).get('/groups').then(response => {
    //         assert(response.body.success, true);
    //         done();
    //     });
    // });

    it('It should pass if it gets a single group [GET /groups/123]', (done) => {
        request(url.baseUrl).get('/groups/123').then(response => {
            assert(response.body.success, true);
            done();
        });
    });

    it('It should pass if it posts a group [POST /group]', (done) => {
        request(url.baseUrl).post('/group').send({
            creatorId: uuid(),
            name: 'Test Group'
        }).expect(response => {
            group.id = response.body.group.id;
            group.creatorId = response.body.group.creator_id;
            assert(response.body.success, true);
        }).end((err) => {
            if (err) return done(err);
            done();
        });
    });

    it('It should pass if it patches a group [PATCH /group]', (done) => {
        request(url.baseUrl).patch('/group').send({
            id: group.id,
            creatorId: group.creatorId,
            name: 'Renamed group'
        }).expect(response => {
            assert(response.body.success, true);
        }).end((err) => {
            if (err) return done(err);
            done();
        });
    });

    it('It should pass if it posts an employee to a group [POST /group/member/add]', (done) => {
        request(url.baseUrl).post('/group/member/add').send({
            groupId: '05da3130-90c6-11e8-957e-7b73bdce866a',
            employeeId: '05c21550-90c6-11e8-957e-7b73bdce866a'
        }).expect(response => {
            assert(response.body.success, true);
        }).end((err) => {
            if (err) return done(err);
            done();
        });
    });

    it('It should pass if it removes a group [DELETE /group]', (done) => {
        request(url.baseUrl).delete('/group').send({
            id: 123
        }).expect(response => {
            assert(response.body.success, true);
        }).end((err) => {
            if (err) return done(err);
            done();
        });
    });
});