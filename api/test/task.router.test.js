const assert = require('assert');

const request = require('supertest');

const common = require('../app/utils/common');

const uuid = require('uuid/v1');

const url = require('../../.constants/config');

describe('GET /', () => {
    it(`It should pass if it returns the AppName [${common.AppName}]`, (done) => {
        request(url.baseUrl)
            .get('/')
            .then(response => {
                assert.equal(response.text, common.AppName);
                done();
            });
    });
});

describe('Test Task Routes', () => {

    let task = {
        id: undefined,
        creatorId: undefined
    };

    it('It should pass if it gets tasks [GET /tasks]', (done) => {
        request(url.baseUrl)
            .get('/tasks')
            .then(response => {
                assert(response.body.success, true);
                done();
            });
    });

    it('It should pass if it gets a single task [GET tasks/123]', (done) => {
        request(url.baseUrl)
            .get('/tasks/123')
            .then(response => {
                assert(response.body.success, true);
                done();
            });
    });

    it('It should pass if it posts a task [POST /task]', (done) => {
        request(url.baseUrl)
            .post('/task')
            .send({
                creatorId: uuid(),
                text: 'Run from test'
            })
            .expect(response => {
                task.id = response.body.task.id;
                task.creatorId = response.body.task.creator_id;
                assert(response.body.success, true);
            })
            .end((err) => {
                if (err)
                    return done(err);
                done();
            });
    });

    it('It should pass if it patches the task [PATCH /task]', (done) => {
        request(url.baseUrl)
            .patch('/task')
            .send({
                id: task.id,
                creatorId: task.creatorId,
                text: 'Updated task from test runner',
                status: 'COMPLETED',
                scope: 'PUBLIC'
            })
            .expect(response => {
                assert(response.body.success, true);
            })
            .end((err) => {
                if (err)
                    return done(err);
                done();
            });
    });

    it('It should pass if it tags an employee to a task [POST /task/tag]', (done) => {
        request(url.baseUrl)
            .post('/task/tag')
            .send({
                employeeId: '046934b0-90c9-11e8-9b4c-536dfef5231b',
                taskId: '04771760-90c9-11e8-9b4c-536dfef5231b'
            })
            .expect(response => {
                assert(response.body.success, true);
            })
            .end((err) => {
                if (err)
                    return done(err);
                done();
            });
    });

    it('It should pass if it assigns a task to a group [POST /task/group/assign]', (done) => {
        request(url.baseUrl)
            .post('/task/group/assign')
            .send({
                groupId: '02321020-911b-11e8-927c-d5edec4a56f8',
                taskId: '04771760-90c9-11e8-9b4c-536dfef5231b'
            })
            .expect(response => {
                assert(response.body.success, true);
            })
            .end((err) => {
                if (err)
                    return done(err);
                done();
            });
    });

    it('It should pass if it removes a task [DELETE /task]', (done) => {
        request(url.baseUrl)
            .delete('/task')
            .send({
                id: 123
            })
            .expect(response => {
                assert(response.body.success, true);
            })
            .end((err) => {
                if (err)
                    return done(err);
                done();
            });
    });

});