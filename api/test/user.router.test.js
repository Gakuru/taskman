const assert = require('assert');

const request = require('supertest');

const common = require('../app/utils/common');

const uuid = require('uuid/v1');

const url = require('../../.constants/config');

describe('Test User Routes', () => {

    let task = {
        id: undefined,
        creatorId: undefined
    };

    it('It should pass if it gets users [GET /users]', (done) => {
        request(url.baseUrl)
            .get('/users')
            .then(response => {
                assert(response.body.success, true);
                done();
            });
    });

    it('It should pass if it gets a single user [GET users/123]', (done) => {
        request(url.baseUrl)
            .get('/users/123')
            .then(response => {
                assert(response.body.success, true);
                done();
            });
    });

    it('It should pass if it posts a user [POST /user]', (done) => {
        request(url.baseUrl)
            .post('/user')
            .send({
                firstName: 'Maina',
                lastName: 'Gakuru',
                name: 'Maina Gakuru',
                email: 'gakurumaina@gmail.com',
                imageUrl: 'https://lh6.googleusercontent.com/-AW2W4oM0L9s/AAAAAAAAAAI/AAAAAAAAH_s/x_wFYumcOQk/s96-c/photo.jpg'
            })
            .expect(response => {
                task.id = response.body.task.id;
                task.creatorId = response.body.task.creator_id;
                assert(response.body.success, true);
            })
            .end((err) => {
                if (err)
                    return done(err);
                done();
            });
    });

});