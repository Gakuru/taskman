export default {
  items : [
    {
      name: 'Dashboard',
      url: '/',
      icon: 'icon-speedometer'
    }, {
      title: true,
      name: 'Todos',
      wrapper: {
        element: '',
        attributes: {}
      },
      class: ''
    }, {
      name: 'Tasks',
      url: '/tasks',
      icon: 'icon-notebook'
    }, {
      title: true,
      name: 'Groups',
      wrapper: {
        element: '',
        attributes: {}
      },
      class: ''
    }, {
      name: 'Groups',
      url: '/groups',
      icon: 'icon-people'
    }
  ]
};
