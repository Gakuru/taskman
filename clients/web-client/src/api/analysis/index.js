import urls from '../urls';

import {tokenId} from '../../utils/token';

const baseUrl = urls.baseUrl;

export const fetchTaskAnalysisData = async(url) => {
    try {
        const response = await fetch(url
            ? url
            : `${baseUrl}/analysis/tasks`, {
            headers: {
                'x-access-token': tokenId
            }
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}