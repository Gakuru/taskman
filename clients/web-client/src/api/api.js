import urls from './urls';

const baseUrl = urls.baseUrl;

export const loginUser = async(user) => {
    try {
        const response = await fetch(`${baseUrl}/user`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}