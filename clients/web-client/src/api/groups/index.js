import urls from '../urls';

import {tokenId} from '../../utils/token';

const baseUrl = urls.baseUrl;

export const fetchGroups = async(url) => {
    try {
        const response = await fetch(url
            ? url
            : `${baseUrl}/groups`, {
            headers: {
                'x-access-token': tokenId
            }
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

export const findGroups = async({param, url}) => {
    try {
        const response = await fetch(url
            ? url
            : `${baseUrl}/groups/find/${param}`, {
            headers: {
                'x-access-token': tokenId
            }
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

export const saveGroup = async(group) => {
    try {
        const response = await fetch(`${baseUrl}/group`, {
            method: group.id
                ? 'PATCH'
                : 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': tokenId
            },
            body: JSON.stringify(group)
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

export const saveGroupMember = async(meber) => {
    try {
        const response = await fetch(`${baseUrl}/group/member/add`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': tokenId
            },
            body: JSON.stringify(meber)
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

export const removeGroup = async(groupId) => {
    try {
        const response = await fetch(`${baseUrl}/group`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': tokenId
            },
            body: JSON.stringify({id: groupId})
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

export const updateGroup = async(group) => {
    try {
        const response = await fetch(`${baseUrl}/group`, {
            method: 'PATCH',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': tokenId
            },
            body: JSON.stringify(group)
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}