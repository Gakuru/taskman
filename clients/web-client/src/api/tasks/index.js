import urls from '../urls';

import {tokenId} from '../../utils/token';

const baseUrl = urls.baseUrl;

export const fetchTasks = async(url) => {
    try {
        const response = await fetch(url
            ? url
            : `${baseUrl}/tasks`, {
            headers: {
                'x-access-token': tokenId
            }
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

export const getUsers = async({param, url}) => {
    try {
        const response = await fetch(url
            ? url
            : `${baseUrl}/users/find/${param}`, {
            headers: {
                'x-access-token': tokenId
            }
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

export const saveTask = async(task) => {
    try {
        const response = await fetch(`${baseUrl}/task`, {
            method: task.id
                ? 'PATCH'
                : 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': tokenId
            },
            body: JSON.stringify(task)
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

export const tagTask = async(tag) => {;
    try {
        const response = await fetch(`${baseUrl}/task/tag`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': tokenId
            },
            body: JSON.stringify(tag)
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

export const groupTask = async(group) => {;
    try {
        const response = await fetch(`${baseUrl}/task/group/assign`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': tokenId
            },
            body: JSON.stringify(group)
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

export const removeTask = async(taskId) => {
    try {
        const response = await fetch(`${baseUrl}/task`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': tokenId
            },
            body: JSON.stringify({id: taskId})
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

export const updateTask = async(task) => {
    try {
        const response = await fetch(`${baseUrl}/task`, {
            method: 'PATCH',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': tokenId
            },
            body: JSON.stringify(task)
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}