import React, {Component} from 'react';

import {connect} from 'react-redux';

import {Link} from 'react-router-dom';

import {Badge, DropdownItem, DropdownMenu, DropdownToggle, Nav} from 'reactstrap';
import PropTypes from 'prop-types';

import {AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler} from '@coreui/react';

import logo from '../../assets/img/brand/logo.png';
import sygnet from '../../assets/img/brand/sygnet.png';

import {user, logOut} from '../../utils/token';

import _ from 'lodash';

const propTypes = {
  children: PropTypes.node
};

const defaultProps = {};

class DefaultHeader extends Component {

  constructor(props) {
    super(props);

    this.state = {
      taskAnalysis: {}
    }

    this.getValue = this
      .countPendingTasks
      .bind(this);
  }

  countPendingTasks(path) {

    const value = _.get(this.props.taskAnalysis.taskAnalysis, path);

    return value
      ? value
      : 0;
  }

  render() {

    // eslint-disable-next-line
    const {children} = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile/>
        <AppNavbarBrand
          full={{
          src: logo,
          width: 120,
          height: 50,
          alt: 'App Logo'
        }}
          minimized={{
          src: sygnet,
          width: 60,
          height: 50,
          alt: 'App Logo'
        }}/>
        <AppSidebarToggler className="d-md-down-none" display="lg"/>

        <Nav className="ml-auto" navbar>
          {/*<NavItem className="d-md-down-none">
            <NavLink href="#">
              <i className="icon-bell"></i>
              <Badge pill color="danger">5</Badge>
      </NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink href="#">
              <i className="icon-list"></i>
            </NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink href="#">
              <i className="icon-location-pin"></i>
            </NavLink>
          </NavItem>*/}
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <img src={user.image_url} className="img-avatar" alt={user.name}/>
            </DropdownToggle>
            <DropdownMenu right style={{
              right: 'auto'
            }}>
              <DropdownItem header tag="div" className="text-center">
                <strong>Account</strong>
              </DropdownItem>
              <DropdownItem>
                <Link to='/tasks'>
                  <i className="fa fa-tasks"></i>
                  Tasks<Badge color="danger">{this.countPendingTasks('status.pending.count')}</Badge>
                </Link>
              </DropdownItem>
              <DropdownItem onClick={logOut}>
                <i className="fa fa-lock"></i>
                Logout
              </DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
        {/*<AppAsideToggler className="d-md-down-none"/> <AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (store) => {
  return {taskAnalysis: store.taskAnalysis}
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default connect(mapStateToProps)(DefaultHeader);