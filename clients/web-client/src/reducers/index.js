import {combineReducers} from 'redux';

import user from '../views/login/reducer/login.reducer';
import tasks, {task} from '../views/tasks/reducer/tasks.reducer';
import groups, {group} from '../views/groups/reducer/groups.reducer';
import {users, tags} from '../views/tasks/reducer/tasks.tags.reducer';
import {taskAnalysis} from '../views/dashboard/reducer/dashboard.reducer';

export default combineReducers({
    user,
    tasks,
    task,
    groups,
    group,
    users,
    tags,
    taskAnalysis
});