import React from 'react';
import Loadable from 'react-loadable'

import DefaultLayout from './containers/DefaultLayout';

function Loading() {
  return <div>Loading...</div>;
}

const Dashboard = Loadable({
  loader: () => import ('./views/dashboard/dashboard'),
  loading: Loading
});

const Tasks = Loadable({
  loader: () => import ('./views/tasks/tasks'),
  loading: Loading
});

const CreateTask = Loadable({
  loader: () => import ('./views/tasks/forms/tasks.create'),
  loading: Loading
});

const TagTask = Loadable({
  loader: () => import ('./views/tasks/tasks.tag'),
  loading: Loading
});

const GroupTask = Loadable({
  loader: () => import ('./views/tasks/tasks.group'),
  loading: Loading
});

const Groups = Loadable({
  loader: () => import ('./views/groups/groups'),
  loading: Loading
});

const CreateGroup = Loadable({
  loader: () => import ('./views/groups/forms/groups.create'),
  loading: Loading
});

const AddGroupMember = Loadable({
  loader: () => import ('./views/groups/groups.addGroupMember'),
  loading: Loading
});

const routes = [
  {
    path: '/',
    exact: true,
    name: 'Home',
    component: DefaultLayout
  }, {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard
  }, {
    path: '/tasks',
    exact: true,
    name: 'Tasks',
    component: Tasks
  }, {
    path: '/tasks/create',
    name: 'Create Task',
    component: CreateTask
  }, {
    path: '/tasks/:taskId/edit',
    name: 'Edit Task',
    component: CreateTask
  }, {
    path: '/tasks/:taskId/tag',
    name: 'Tag Task',
    component: TagTask
  }, {
    path: '/tasks/:taskId/group',
    name: 'Group Task',
    component: GroupTask
  }, {
    path: '/groups',
    exact: true,
    name: 'Groups',
    component: Groups
  }, {
    path: '/groups/create',
    name: 'Create Group',
    component: CreateGroup
  }, {
    path: '/groups/:groupId/edit',
    name: 'Edit Task',
    component: CreateGroup
  }, {
    path: '/groups/:groupId/addmember',
    name: 'Add Group Member',
    component: AddGroupMember
  }
];

export default routes;
