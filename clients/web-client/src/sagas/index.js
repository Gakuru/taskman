import {fork} from 'redux-saga/effects';

import {loginUserSaga} from '../views/login/sagas/login.saga';

import tasksSaga from '../views/tasks/sagas/tasks.saga';
import groupsSaga from '../views/groups/sagas/groups.saga';
import tasksTagSaga from '../views/tasks/sagas/tasks.tag.saga';
import DashboardSaga from '../views/dashboard/sagas/dashboard.saga';

export default function * rootSaga() {
    yield[fork(loginUserSaga),
        fork(tasksSaga),
        fork(groupsSaga),
        fork(tasksTagSaga),
        fork(DashboardSaga)];
}