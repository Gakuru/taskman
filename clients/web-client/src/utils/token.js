export const TOKEN_NAME = 'taskmanToken'
export const token = localStorage.getItem(TOKEN_NAME);

export const taskmanToken = token
    ? token
    : undefined;

export const tokenId = token
    ? JSON
        .parse(token)
        .tokenId
    : undefined;

export const user = token
    ? JSON
        .parse(token)
        .userProfile
    : undefined;

export const authed = taskmanToken
    ? true
    : false;

export const logOut = () => {
    localStorage.removeItem(TOKEN_NAME);
    window.location = '/login';
};

export const isOAuthed = (code) => {
    if (code === 403) {
        localStorage.removeItem(TOKEN_NAME);
        window.location = '/';
    }
    return true
}