import React, {Component} from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Button,
  Row,
  Col,
  CardFooter
} from 'reactstrap';

import {Link} from 'react-router-dom';

class RemoveReservation extends Component {

  constructor(props) {
    super(props);

    this.handleRemove = this
      .handleRemove
      .bind(this);

  }

  handleRemove = () => {
    console.info('submit');
  }

  render() {
    return (
      <div className="animated fadeIn">

        <Row>
          <Col xs="12" md="12" lg="12">
            <Card>
              <CardHeader>
                <i className="fa fa-trash"></i>
                Remove Reservation
              </CardHeader>
              <CardBody>

                <div>
                  Are you sure you want to remove this reservation?
                </div>

              </CardBody>

              <CardFooter>
                <div className="ml-auto">
                  <Link to={`/reservations`} className="btn btn-primary">
                    <i className="icon-ban"></i>
                    &nbsp; Cancel
                  </Link>
                  &nbsp;
                  <Button type="button" color="danger" onClick={this.handleRemove}>
                    <i className="fa fa-trash"></i>
                    &nbsp; Remove
                  </Button>
                </div>
              </CardFooter>

            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default RemoveReservation;
