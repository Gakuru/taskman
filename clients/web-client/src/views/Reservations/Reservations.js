import React, {Component} from 'react';

// import {bindActionCreators} from 'redux'; import {connect} from
// 'react-redux'; import { requestGetReservations } from
// '../../actions/reservations';

import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table
} from 'reactstrap';

import {Link} from 'react-router-dom';

import moment from 'moment';

const itemListNumber = (i, pager) => {
  return (pager
    ? (((pager.currStartAt - 1) * (pager.pageSize) + ((++i))))
    : ((++i)))
}

class Reservations extends Component {

  constructor(props) {
    super(props);

    this.state = {
      dateFormat: 'YYYY-MM-DD HH:mm:ss'
    }

    this.renderReservations = this
      .renderReservations
      .bind(this);

    this.renderCancelLink = this
      .renderCancelLink
      .bind(this);

    this.renderRows = this
      .renderRows
      .bind(this);
    this.getReservations = this
      .getReservations
      .bind(this);
  }

  componentDidMount = () => {
    // this.props.requestGetReservations();
  }

  getReservations(url) {
    // this.props.requestGetReservations(url);
  }

  renderCancelLink = (reservation) => {
    if (reservation.status === 'paid') {
      return (
        <span>
          &emsp;
          <Link
            to={`/reservations/${reservation
            .reservationId
            .toLowerCase()}/cancel`}
            className="text-warning">
            <i className="icon-ban"></i>
          </Link>
        </span>
      )
    } else {
      return (
        <span>&emsp;&emsp;</span>
      )
    }
  }

  renderReservations = () => {
    if (this.props.reservations) {
      const {reservations, pager} = this.props.reservations;
      return (
        <Table hover striped responsive>
          <thead>
            <tr>
              <th>
                <div>#</div>
              </th>
              <th>
                <div>Reservation ID</div>
              </th>
              <th>
                <div>Reserver</div>
              </th>
              <th>
                <div>Mobile No</div>
              </th>
              <th>
                <div>Amount paid</div>
              </th>
              <th>
                <div>Period</div>
              </th>
              <th>
                <div>Status</div>
              </th>
              <th>
                <div className="float-right">Actions</div>
              </th>
            </tr>
          </thead>
          <tbody>
            {this.renderRows(reservations, pager)}
          </tbody>
        </Table>
      )
    }
  }

  renderRows = (reservations, pager) => {
    if (reservations) {
      return reservations.map((reservation, i) => {
        return (
          <tr key={i}>
            <td>
              <div>{itemListNumber(i, pager)}</div>
            </td>
            <td>
              <div>{reservation.reservationId}</div>
            </td>
            <td>
              <div>{reservation.reservoir}</div>
            </td>
            <td>
              <div>{reservation.reservoirMobileNo}</div>
            </td>
            <td>
              <div>{reservation.amountPaid}</div>
            </td>
            <td>
              <div>
                {`${moment(reservation.period.from).format(this.state.dateFormat)} to ${moment(reservation.period.to).format(this.state.dateFormat)}`}
              </div>
            </td>
            <td>
              <div>{reservation.status}</div>
            </td>
            <td>
              <div className="float-right">
                <Link
                  to={{
                  pathname: `/reservations/${reservation
                    .reservationId
                    .toLowerCase()}/edit`,
                  state: {
                    reservation: reservation
                  }
                }}>
                  <i className="icon-pencil"></i>
                </Link>
                {this.renderCancelLink(reservation)}
                &emsp;
                <Link
                  to={`/reservations/${reservation
                  .reservationId
                  .toLowerCase()}/remove`}
                  className="text-danger">
                  <i className="icon-trash"></i>
                </Link>
              </div>
            </td>
          </tr>
        );
      })
    }
  }

  render() {
    return (
      <div className="animated fadeIn">

        <Card>
          <CardHeader>
            <Row className="align-items-center">
              <Col col="1" sm="2" md="2" className="">
                <Link to={`/reservations/create`} className="btn btn-outline-primary">
                  <i className="icon-plus"></i>
                  &nbsp; New Reservation
                </Link>
              </Col>
            </Row>
          </CardHeader>
        </Card>

        <Row>
          <Col xs="12" md="12" lg="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i>
                Reservations
              </CardHeader>
              <CardBody
                style={{
                height: '45vh',
                overflow: 'auto'
              }}>
                {this.renderReservations()}
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

// const mapStateToProps = (store) => {   return {reservations:
// store.reservations} } const mapDispatchToProps = (dispatch) =>
// bindActionCreators({   requestGetReservations }, dispatch); export default
// connect(mapStateToProps, mapDispatchToProps)(Reservations);
export default Reservations