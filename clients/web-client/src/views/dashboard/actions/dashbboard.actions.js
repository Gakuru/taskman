import {REQUEST_TASK_ANALYSIS, TASK_ANALYSIS_SUCCESS} from "../types/dashboard.types";

export const requestGetTaskAnalysis = () => ({type: REQUEST_TASK_ANALYSIS});
export const getTaskAnalysisSuccess = taskAnalysis => ({type: TASK_ANALYSIS_SUCCESS, taskAnalysis});