import React, {Component} from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {Card, CardBody, Col, Row} from 'reactstrap';

import {requestGetTaskAnalysis} from './actions/dashbboard.actions';

import _ from 'lodash';

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      taskAnalysis: {}
    }

    this.getValue = this
      .getValue
      .bind(this);
  }

  componentDidMount() {
    this
      .props
      .requestGetTaskAnalysis();
  }

  componentDidUpdate(previousProps) {
    if (previousProps.taskAnalysis !== this.props.taskAnalysis) {
      const {taskAnalysis} = this.props.taskAnalysis;
      this.setState({taskAnalysis});
    }
  }

  getValue = (path) => {

    const value = _.get(this.state.taskAnalysis, path);

    return value
      ? value
      : 0;
  }

  render() {

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="6" lg="4">
            <Card className="text-white bg-info">
              <CardBody className="pb-0">
                <div className="text-value">
                  Task's Status
                </div>
                <div className="row">
                  <div className="col-12"><hr/></div>
                  <div className="col-5">
                    Pending
                  </div>
                  <div className="col-7">
                    <div className="float-right">
                      {this.getValue('status.pending.count')}
                    </div>
                  </div>
                  <div className="col-12"><hr/></div>
                  <div className="col-5">
                    Complete
                  </div>
                  <div className="col-7">
                    <div className="float-right">
                      {this.getValue('status.complete.count')}
                    </div>
                  </div>
                </div>
              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="4">
            <Card className="text-white bg-warning">
              <CardBody className="pb-0">
                <div className="text-value">
                  Your Tasks
                </div>
                <div className="row">
                  <div className="col-12"><hr/></div>
                  <div className="col-5">
                    Today
                  </div>
                  <div className="col-7">
                    <div className="float-right">
                      {this.getValue('time.today.count')}
                    </div>
                  </div>
                  <div className="col-12"><hr/></div>
                  <div className="col-5">
                    This week
                  </div>
                  <div className="col-7">
                    <div className="float-right">
                      {this.getValue('time.thisweek.count')}
                    </div>
                  </div>
                </div>
              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="4">
            <Card className="text-white bg-primary">
              <CardBody className="pb-0">
                <div className="text-value">
                  Task's Scope
                </div>
                <div className="row">
                  <div className="col-12"><hr/></div>
                  <div className="col-5">
                    Private
                  </div>
                  <div className="col-7">
                    <div className="float-right">
                      {this.getValue('scope.private.count')}
                    </div>
                  </div>
                  <div className="col-12"><hr/></div>
                  <div className="col-5">
                    Public
                  </div>
                  <div className="col-7">
                    <div className="float-right">
                      {this.getValue('scope.public.count')}
                    </div>
                  </div>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (store) => {
  return {taskAnalysis: store.taskAnalysis}
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
  requestGetTaskAnalysis
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);