import {TASK_ANALYSIS_SUCCESS} from "../types/dashboard.types";

export const taskAnalysis = (state = {}, {type, taskAnalysis}) => {
    switch (type) {
        case TASK_ANALYSIS_SUCCESS:
            {
                return taskAnalysis;
            }

        default:
            {
                return state;
            }
    }
}