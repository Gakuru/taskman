import {put, call, takeLatest, all} from 'redux-saga/effects';

import {REQUEST_TASK_ANALYSIS} from '../types/dashboard.types';
import {getTaskAnalysisSuccess} from '../actions/dashbboard.actions';

import {fetchTaskAnalysisData} from '../../../api/analysis';
import {isOAuthed} from '../../../utils/token';

function * _fetchTaskAnalysisData({url}) {
    try {
        const data = yield call(fetchTaskAnalysisData, url);
        if (isOAuthed(data.code)) 
            if (data.success) 
                yield put(getTaskAnalysisSuccess(data));
            }
        catch (e) {
        console.log(e);
    }
}

function * fetchTaskAnalysisSaga() {
    yield takeLatest(REQUEST_TASK_ANALYSIS, _fetchTaskAnalysisData);
}

export default function * groupsSaga() {
    yield all([fetchTaskAnalysisSaga()]);
}
