import {
    REQUEST_GET_GROUPS,
    GET_GROUPS_SUCCESS,
    REQUEST_SAVE_GROUP,
    SAVE_GROUP_SUCCESS,
    REQUEST_REMOVE_GROUP,
    REMOVE_GROUP_SUCCESS,
    REQUEST_ADD_MEMBER,
    ADD_MEMBER_SUCCESS,
    REQUEST_FIND_GROUPS
} from '../types/groups.types';

export const requestGetGroups = userId => ({type: REQUEST_GET_GROUPS, userId});
export const getGroupsSuccess = groups => ({type: GET_GROUPS_SUCCESS, groups});

export const requestSaveGroup = group => ({type: REQUEST_SAVE_GROUP, group});
export const saveGroupSuccess = group => ({type: SAVE_GROUP_SUCCESS, group});

export const requestRemoveGroup = groupId => ({type: REQUEST_REMOVE_GROUP, groupId});
export const removeGroupSuccess = groups => ({type: REMOVE_GROUP_SUCCESS, groups});

export const requestAddMember = member => ({type: REQUEST_ADD_MEMBER, member});
export const addMemberSuccess = members => ({type: ADD_MEMBER_SUCCESS, members});

export const requestFindGroups = param => ({type: REQUEST_FIND_GROUPS, param});
export const findGroupsSuccess = groups => ({type: GET_GROUPS_SUCCESS, groups});