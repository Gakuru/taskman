import React from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {
    FormGroup,
    Label,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    Row,
    Col,
    Alert
} from 'reactstrap';

import {Link} from 'react-router-dom';

import {user} from '../../../utils/token';

import {requestSaveGroup} from '../actions/groups.actions';

import {object} from '../../../utils/object';

class CreateGroup extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            alertVisible: false,
            groupTtile: '',
            editing: true,
            group: {
                name: '',
                creatorId: user.id
            }
        };

        this.handleSubmit = this
            .handleSubmit
            .bind(this);

        this.handleInputChange = this
            .handleInputChange
            .bind(this);

        this.onDismiss = this
            .onDismiss
            .bind(this);

    }

    componentDidMount() {
        if (object.isEmpty(this.props.match.params) && this.props.location.state === undefined) {
            this.setState({editing: false});
        } else if (!object.isEmpty(this.props.match.params) && this.props.location.state === undefined) {
            // An edit with no data, get data from the db then set state
            console.info('async call');
        } else {
            let group = this.props.location.state.group;
            let groupTtile = group.name
            this.setState({group});
            this.setState({groupTtile});
            this.setState({editing: true});
        }
    }

    componentDidUpdate(previousProps) {
        if (previousProps.group !== this.props.group) {
            this.setState({alertVisible: this.props.group.success});
            setTimeout(() => {
                this.onDismiss();
            }, 3000);
        }
    }

    handleInputChange = (event) => {
        let group = this.state.group;
        group[event.target.name] = event.target.value;
        this.setState({group});
    }

    onDismiss = () => {
        this.setState({alertVisible: false});
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this
            .props
            .requestSaveGroup(this.state.group);

        let group = this.state.group;
        group.name = '';
        this.setState({group});
    }

    render() {

        return (

            <div className="animated fadeIn">
                <Row>
                    <Col xs="12" md="12" lg="12">
                        <form onSubmit={this.handleSubmit}>
                            <Card>
                                <CardHeader>
                                    <Row>
                                        <Col>
                                            <i
                                                className={this.state.editing
                                                ? 'fa fa-edit'
                                                : 'fa fa-plus'}/>
                                            &nbsp; {this.state.editing
                                                ? `Edit Group - ${this.state.taskTtile}`
                                                : 'New Group'}
                                        </Col>
                                        <Col>
                                            <Alert
                                                className="float-right"
                                                color="info"
                                                isOpen={this.state.alertVisible}
                                                toggle={() => this.onDismiss()}>
                                                Group {this.state.editing
                                                    ? 'updated'
                                                    : 'saved'}....
                                            </Alert>
                                        </Col>
                                    </Row>
                                </CardHeader>
                                <CardBody>
                                    <FormGroup>
                                        <Label for="text">Group Name</Label>
                                        <input
                                            type="text"
                                            name="name"
                                            onChange={(event) => this.handleInputChange(event)}
                                            value={this.state.group.name}
                                            className="form-control"/>
                                    </FormGroup>
                                </CardBody>
                                <CardFooter>
                                    <Link to={`/groups`} className="btn btn-outline-danger">
                                        <i className="fa fa-times"/>&nbsp;Cancel</Link>
                                    &nbsp;
                                    <Button outline color="primary"><i className="fa fa-plus"/>&nbsp;Save</Button>
                                </CardFooter>
                            </Card>
                        </form>
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = (store) => {
    return {group: store.group}
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    requestSaveGroup
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CreateGroup);