import React from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {Link} from 'react-router-dom';

import {
    Card,
    CardHeader,
    CardBody,
    Row,
    Col,
    Button
} from 'reactstrap';

import {requestGetUsers} from '../tasks/actions/tasks.actions';

import {requestAddMember} from './actions/groups.actions';

class AddGroupMember extends React.Component {

    constructor(props) {
        super(props);

        this.groupId = props.match.params.groupId;

        this.state = {
            isTyping: false,
            users: []
        }

        this.getUsers = this
            .getUsers
            .bind(this);

        this.renderSuggestBox = this
            .renderSuggestBox
            .bind(this);

        this.renderSuggestions = this
            .renderSuggestions
            .bind(this);

        this.addMember = this.addMember = this
            .addMember
            .bind(this);
    }

    componentDidUpdate(previousProps) {
        if (previousProps.users !== this.props.users) {
            this.setState({users: this.props.users.users});
        }
    }

    getUsers = event => {
        let value = event.target.value;
        let isTyping = value.length > 0
            ? true
            : false;
        this
            .props
            .requestGetUsers(value);

        this.setState({isTyping});
    }

    addMember = (userId, groupId) => {
        this
            .props
            .requestAddMember({userId, groupId});
    }

    renderSuggestBox = () => {
        if (this.state.isTyping) {
            return (
                <div>
                    {this.renderSuggestions()}
                </div>
            );
        }
    }

    renderSuggestions = () => {
        if (this.state.users) {
            const {users} = this.state;
            if (users) {
                return users.map(user => {
                    return (
                        <div
                            key={user.id}
                            style={{
                            marginTop: 10,
                            padding: 5,
                            borderBottom: 'solid',
                            borderBottomColor: '#f1f1f1',
                            borderBottomWidth: 1
                        }}>
                            <Row>
                                <Col>
                                    <div>
                                        <img
                                            className="rounded-circle"
                                            src={user.image_url}
                                            alt={user.last_name}
                                            width={48}
                                            height={48}/>
                                    </div>
                                </Col>
                                <Col>
                                    <div
                                        style={{
                                        paddingTop: 12
                                    }}>
                                        {user.name}
                                    </div>
                                </Col>
                                <Col>
                                    <div className="float-right">
                                        <Button
                                            outline
                                            color="primary"
                                            onClick={() => this.addMember(user.id, this.groupId)}>
                                            Add
                                        </Button>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    )
                });
            }
        }
    }

    render() {
        return (

            <div className="animated fadeIn">
                <Row>
                    <Col xs="12" md="12" lg="12">
                        <Card>
                            <CardHeader>
                                <i className="fa fa-user-plus"/>
                                &nbsp; Add Member
                            </CardHeader>
                            <CardBody>
                                <div className="breadcrumb">
                                    <Link to={`/tasks`} className="btn btn-outline-primary">
                                        <i className="fa fa-arrow-left"/>
                                        &nbsp; Back
                                    </Link>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="tagusers">Select Employees to add Group:</label>
                                    <input className="form-control" onChange={event => this.getUsers(event)}/> {this.renderSuggestBox()}
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

}

const mapStateToProps = (store) => {
    return {users: store.users}
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    requestGetUsers,
    requestAddMember
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AddGroupMember);