import React from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {Link} from 'react-router-dom';

import {
    Card,
    CardHeader,
    CardBody,
    Table,
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Col,
    Row
} from 'reactstrap';

import {requestGetGroups, requestRemoveGroup} from './actions/groups.actions';

const object = {
    isEmpty: (obj) => {
        return (Object.keys(obj).length === 0 && obj.constructor === Object
            ? true
            : false);
    }
}

class Groups extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            removing: false,
            modal: true
        };

        this.renderGroups = this
            .renderGroups
            .bind(this);

        this.toggleModal = this
            .toggleModal
            .bind(this);

        this.renderModal = this
            .renderModal
            .bind(this);
    }

    componentDidMount() {
        this
            .props
            .requestGetGroups();
    }

    componentDidUpdate(previousProps) {
        if (previousProps.groups !== this.props.groups) {
            this.setState({
                modal: !this.state.modal,
                removing: false
            });
        }
    }

    toggleModal(group) {
        this.setState({
            modal: !this.state.modal,
            group: group
        });
    }

    removeGroup() {
        this
            .props
            .requestRemoveGroup(this.state.group.id);
        this.setState({removing: true});
    }

    renderModal() {
        if (this.state.group) {
            return (
                <div>
                    <Modal isOpen={this.state.modal} toggle={this.toggle}>
                        <ModalHeader toggle={this.toggleModal}>Remove Group</ModalHeader>
                        <ModalBody>
                            Remove group: {this.state.group.name}
                        </ModalBody>
                        <ModalFooter>
                            <Button outline color="primary" onClick={this.toggleModal}>Cancel</Button>
                            <Button
                                outline
                                color={this.state.removing
                                ? 'warning'
                                : 'danger'}
                                disabled={this.state.removing}
                                onClick={this
                                .removeGroup
                                .bind(this)}>
                                {this.state.removing
                                    ? 'Removing...'
                                    : 'Remove'}
                            </Button>
                        </ModalFooter>
                    </Modal>
                </div>
            )
        }
    }

    renderGroups = () => {
        if (!object.isEmpty(this.props.groups)) {
            const {groups} = this.props.groups;
            return (
                <Table hover striped responsive>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Group</th>
                            <th>Creator</th>
                            <th>Date Created</th>
                            <th>
                                <div className="float-right">Actions</div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {groups.map((group, i) => {
                            return (
                                <tr key={group.id}>
                                    <td>{(++i)}</td>
                                    <td>{group.name}</td>
                                    <td>{group.creator}</td>
                                    <td>{group.created_at}</td>
                                    <td>
                                        <div className="float-right">
                                            <Link
                                                to={`/groups/${group.id}/addmember`}
                                                className="btn btn-outline-info btn-sm"
                                                title="Add members to this group">
                                                <i className="fa fa-user-plus"/>
                                            </Link>
                                            &nbsp;
                                            <Link
                                                className="btn btn-outline-primary btn-sm"
                                                title="Edit this task"
                                                to={{
                                                pathname: `/groups/${group.id}/edit`,
                                                state: {
                                                    group: group
                                                }
                                            }}>
                                                <i className="fa fa-edit"></i>
                                            </Link>
                                            &nbsp;
                                            <Button
                                                outline
                                                color="danger"
                                                size="sm"
                                                title="Delete this group"
                                                onClick={() => this.toggleModal(group)}>
                                                <i className="fa fa-trash"/>
                                            </Button>
                                        </div>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </Table>
            );
        }
    }

    render() {
        return (

            <div className="animated fadeIn">
                {this.renderModal()}
                <Card>
                    <CardHeader>
                        <Row className="align-items-center">
                            <Col col="1" sm="2" md="2" className="">
                                <Link to={`/groups/create`} className="btn btn-outline-primary">
                                    <i className="icon-plus"></i>
                                    &nbsp; New Group
                                </Link>
                            </Col>
                        </Row>
                    </CardHeader>
                </Card>

                <Row>
                    <Col xs="12" md="12" lg="12">
                        <Card>
                            <CardHeader>
                                <i className="icon-people"></i>
                                Groups You Manage
                            </CardHeader>
                            <CardBody>
                                {this.renderGroups()}
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (store) => {
    return {groups: store.groups}
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    requestGetGroups,
    requestRemoveGroup
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Groups);