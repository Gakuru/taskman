import {GET_GROUPS_SUCCESS, SAVE_GROUP_SUCCESS, REMOVE_GROUP_SUCCESS, ADD_MEMBER_SUCCESS} from "../types/groups.types";

export default(state = {}, {type, groups}) => {
    switch (type) {
        case GET_GROUPS_SUCCESS:
            {
                return groups;
            }
        case REMOVE_GROUP_SUCCESS:
            {
                return groups;
            }
        default:
            {
                return state;
            }
    }
}

export const group = (state = {}, {type, group}) => {
    switch (type) {
        case SAVE_GROUP_SUCCESS:
            {
                return group;
            }
        default:
            {
                return state;
            }
    }
}

export const groupMembers = (state = {}, {type, members}) => {
    switch (type) {
        case ADD_MEMBER_SUCCESS:
            {
                return members;
            }
        default:
            {
                return state;
            }
    }
}