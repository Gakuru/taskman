import {put, call, takeLatest, all} from 'redux-saga/effects';

import {REQUEST_GET_GROUPS, REQUEST_SAVE_GROUP, REQUEST_REMOVE_GROUP, REQUEST_ADD_MEMBER, REQUEST_FIND_GROUPS} from '../types/groups.types';
import {getGroupsSuccess, saveGroupSuccess, removeGroupSuccess, addMemberSuccess, findGroupsSuccess} from '../actions/groups.actions';

import {fetchGroups, findGroups, saveGroup, removeGroup, saveGroupMember} from '../../../api/groups';
import {isOAuthed} from '../../../utils/token';

function * _fetchGroups({url}) {
    try {
        const data = yield call(fetchGroups, url);
        if (isOAuthed(data.code)) 
            if (data.success) 
                yield put(getGroupsSuccess(data));
            }
        catch (e) {
        console.log(e);
    }
}

function * _findGroups({param, url}) {
    try {
        const data = yield call(findGroups, {param, url});
        if (isOAuthed(data.code)) 
            if (data.success) 
                yield put(findGroupsSuccess(data));
            }
        catch (e) {
        console.log(e);
    }
}

function * _saveGroup({group}) {
    try {
        const data = yield call(saveGroup, group);
        if (isOAuthed(data.code)) 
            if (data.success) 
                yield put(saveGroupSuccess(data));
            }
        catch (e) {
        console.log(e);
    }
}

function * _saveGroupMember({member}) {
    try {
        const data = yield call(saveGroupMember, member);
        if (isOAuthed(data.code)) 
            if (data.success) 
                yield put(addMemberSuccess(data));
            }
        catch (e) {
        console.log(e);
    }
}

function * _removegroup({groupId}) {
    try {
        const data = yield call(removeGroup, groupId);
        if (isOAuthed(data.code)) 
            if (data.success) 
                yield put(removeGroupSuccess(data));
            }
        catch (e) {
        console.log(e);
    }
}

function * fetchGroupsSaga() {
    yield takeLatest(REQUEST_GET_GROUPS, _fetchGroups);
}

function * findGroupsSaga() {
    yield takeLatest(REQUEST_FIND_GROUPS, _findGroups);
}

function * saveGroupSaga() {
    yield takeLatest(REQUEST_SAVE_GROUP, _saveGroup);
}

function * removeGroupSaga() {
    yield takeLatest(REQUEST_REMOVE_GROUP, _removegroup);
}

function * saveGroupMemberSaga() {
    yield takeLatest(REQUEST_ADD_MEMBER, _saveGroupMember);
}

export default function * groupsSaga() {
    yield all([fetchGroupsSaga(), findGroupsSaga(), saveGroupSaga(), removeGroupSaga(), saveGroupMemberSaga()]);
}
