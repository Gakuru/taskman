import React, { Component } from 'react';
import {
    Card,
    CardBody,
    Col,
    Container,
    InputGroup,
    Row
} from 'reactstrap';

import { Redirect } from 'react-router-dom';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { GoogleLogin } from 'react-google-login';

import { requestLoginUser } from './actions/login.actions';

import { authed } from '../../utils/token';

class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {}

        this.responseGoogle = this
            .responseGoogle
            .bind(this);
    }

    componentDidUpdate(previousProps) {
        if (previousProps.user !== this.props.user) {
            const { user } = this.props.user;
            localStorage.setItem('taskmanToken', JSON.stringify({ tokenId: user.token, userProfile: user.user }));
            // this.setState({authed: true});
            window.location = '/';
        }
    }

    responseGoogle = (response) => {
        const { familyName, givenName, name, email, imageUrl } = response.profileObj;
        this
            .props
            .requestLoginUser({ firstName: givenName, lastName: familyName, name: name, email: email, imageUrl: imageUrl });

    }

    render() {

        if (authed) {
            return <Redirect to={`/`} />
        } else {
            return (
                <div className="app flex-row align-items-center">
                    <Container>
                        <Row className="justify-content-center">
                            <Col md="5">
                                <Card className="p-4">
                                    <CardBody>
                                        <h1>Login</h1>
                                        <p className="text-muted">Sign In to your account</p>
                                        <InputGroup className="mb-3">
                                            <GoogleLogin
                                                clientId="88209892353-h6suu8egd8sn24oj7lq960r9ebdp4c3a.apps.googleusercontent.com"
                                                onSuccess={this.responseGoogle}
                                                onFailure={this.responseGoogle}>
                                                <i className="fa fa-google" />
                                                &nbsp; Login with Google
                                            </GoogleLogin>
                                        </InputGroup>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </div>
            );
        }
    }
}

const mapStateToProps = (store) => {
    return { user: store.user }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    requestLoginUser
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
