import {LOGIN_USER_SUCCESS} from "../types/login.types";

export default(state = {}, {type, user}) => {
    switch (type) {
        case LOGIN_USER_SUCCESS:
            {
                return user;
            }
        default:
            {
                return state;
            }
    }
}