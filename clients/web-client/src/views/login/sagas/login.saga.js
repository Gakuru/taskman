import {put, call, takeLatest} from 'redux-saga/effects';

import {REQUEST_LOGIN_USER} from '../types/login.types';
import {loginUserSuccess} from '../actions/login.actions';

import {loginUser} from '../../../api/api';

function * _loginUser({user}) {
    try {
        //API Call
        const data = yield call(loginUser, user);
        if (data.success) {
            yield put(loginUserSuccess(data));
        }

    } catch (e) {
        // Catch error yield put(receiveGetCourses(courses));
        console.log(e);
    }
}

export function * loginUserSaga() {
    yield takeLatest(REQUEST_LOGIN_USER, _loginUser);
}
