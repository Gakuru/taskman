import {
    REQUEST_GET_TASKS,
    GET_TASKS_SUCCESS,
    REQUEST_SAVE_TASK,
    SAVE_TASK_SUCCESS,
    REQUEST_REMOVE_TASK,
    REMOVE_TASK_SUCCESS,
    REQUEST_GET_USERS,
    GET_USERS_SUCCESS,
    REQUEST_TAG_TASK,
    TAG_TASK_SUCCESS,
    REQUEST_GROUP_TASK,
    GROUP_TASK_SUCCESS
} from '../types/tasks.types';

export const requestGetTasks = userId => ({type: REQUEST_GET_TASKS, userId});
export const getTasksSuccess = tasks => ({type: GET_TASKS_SUCCESS, tasks});

export const requestSaveTask = task => ({type: REQUEST_SAVE_TASK, task});
export const saveTaskSuccess = task => ({type: SAVE_TASK_SUCCESS, task});

export const requestRemoveTask = taskId => ({type: REQUEST_REMOVE_TASK, taskId});
export const removeTaskSuccess = tasks => ({type: REMOVE_TASK_SUCCESS, tasks});

export const requestGetUsers = param => ({type: REQUEST_GET_USERS, param});
export const getUsersSuccess = users => ({type: GET_USERS_SUCCESS, users});

export const requestTagTask = tag => ({type: REQUEST_TAG_TASK, tag});
export const tagTaskSuccess = tag => ({type: TAG_TASK_SUCCESS, tag});

export const requestGroupTask = group => ({type: REQUEST_GROUP_TASK, group});
export const groupTaskSuccess = group => ({type: GROUP_TASK_SUCCESS, group});