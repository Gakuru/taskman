import React from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {
    FormGroup,
    Label,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    ButtonGroup,
    Row,
    Col,
    Alert
} from 'reactstrap';

import {Link} from 'react-router-dom';

import {user} from '../../../utils/token';

import {requestSaveTask} from '../actions/tasks.actions';

import {object} from '../../../utils/object';

class CreateTask extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            alertVisible: false,
            editing: true,
            taskTtile: '',
            task: {
                text: '',
                scope: 'PRIVATE',
                status: 'PENDING',
                creatorId: user.id
            }
        };

        this.onRadioBtnClick = this
            .onRadioBtnClick
            .bind(this);

        this.handleSubmit = this
            .handleSubmit
            .bind(this);

        this.handleInputChange = this
            .handleInputChange
            .bind(this);

        this.onDismiss = this
            .onDismiss
            .bind(this);

        this.renderStatus = this
            .renderStatus
            .bind(this);

    }

    componentDidMount() {
        if (object.isEmpty(this.props.match.params) && this.props.location.state === undefined) {
            this.setState({editing: false});
        } else if (!object.isEmpty(this.props.match.params) && this.props.location.state === undefined) {
            // An edit with no data, get data from the db then set state
            console.info('async call');
        } else {
            let task = this.props.location.state.task;
            let taskTtile = task.text
            this.setState({task});
            this.setState({taskTtile});
            this.setState({editing: true});
        }
    }

    componentDidUpdate(previousProps) {
        if (previousProps.task !== this.props.task) {
            this.setState({alertVisible: this.props.task.success});
            setTimeout(() => {
                this.onDismiss();
            }, 3000);
        }
    }

    onRadioBtnClick = (forItem, rSelected) => {
        let task = this.state.task;
        task[forItem] = rSelected;
        this.setState({task});
    }

    handleInputChange = (event) => {
        let task = this.state.task;
        task[event.target.name] = event.target.value;
        this.setState({task});
    }

    onDismiss = () => {
        this.setState({alertVisible: false});
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this
            .props
            .requestSaveTask(this.state.task);
        let task = this.state.task;
        task.text = '';
        task.scope = 'PRIVATE';
        task.status = 'PENDING';
        this.setState({task});
    }

    renderStatus() {
        if (this.state.editing) {
            return (
                <FormGroup>
                    <Label for="status">Status</Label>
                    <br/>
                    <ButtonGroup>
                        <Button
                            outline
                            color="primary"
                            onClick={() => this.onRadioBtnClick('status', 'PENDING')}
                            active={this.state.task.status === 'PENDING'}>Pending</Button>
                        <Button
                            outline
                            color="success"
                            onClick={() => this.onRadioBtnClick('status', 'COMPLETE')}
                            active={this.state.task.status === 'COMPLETE'}>Complete</Button>
                    </ButtonGroup>
                </FormGroup>
            )
        }
    }

    render() {

        return (

            <div className="animated fadeIn">
                <Row>
                    <Col xs="12" md="12" lg="12">
                        <form onSubmit={this.handleSubmit}>
                            <Card>
                                <CardHeader>
                                    <Row>
                                        <Col>
                                            <i
                                                className={this.state.editing
                                                ? 'fa fa-edit'
                                                : 'fa fa-plus'}/>
                                            &nbsp; {this.state.editing
                                                ? `Edit Task - ${this.state.taskTtile}`
                                                : 'New Task'}
                                        </Col>
                                        <Col>
                                            <Alert
                                                className="float-right"
                                                color="info"
                                                isOpen={this.state.alertVisible}
                                                toggle={() => this.onDismiss()}>
                                                Task {this.state.editing
                                                    ? 'updated'
                                                    : 'saved'}....
                                            </Alert>
                                        </Col>
                                    </Row>
                                </CardHeader>
                                <CardBody>
                                    <FormGroup>
                                        <Label for="text">Task</Label>
                                        <input
                                            type="text"
                                            name="text"
                                            onChange={(event) => this.handleInputChange(event)}
                                            value={this.state.task.text}
                                            className="form-control"/>
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="scope">Scope</Label>
                                        <br/>
                                        <ButtonGroup>
                                            <Button
                                                outline
                                                color="primary"
                                                onClick={() => this.onRadioBtnClick('scope', 'PRIVATE')}
                                                active={this.state.task.scope === 'PRIVATE'}>Private</Button>
                                            <Button
                                                outline
                                                color="warning"
                                                onClick={() => this.onRadioBtnClick('scope', 'PUBLIC')}
                                                active={this.state.task.scope === 'PUBLIC'}>Public</Button>
                                        </ButtonGroup>
                                    </FormGroup>
                                    {this.renderStatus()}
                                </CardBody>
                                <CardFooter>
                                    <Link to={`/tasks`} className="btn btn-outline-danger">
                                        <i className="fa fa-times"/>&nbsp;Cancel</Link>
                                    &nbsp;
                                    <Button outline color="primary"><i className="fa fa-plus"/>&nbsp;Save</Button>
                                </CardFooter>
                            </Card>
                        </form>
                    </Col>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (store) => {
    return {task: store.task}
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    requestSaveTask
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CreateTask);