import {GET_TASKS_SUCCESS, SAVE_TASK_SUCCESS, REMOVE_TASK_SUCCESS} from "../types/tasks.types";

export default(state = {}, {type, tasks}) => {
    switch (type) {
        case GET_TASKS_SUCCESS:
            {
                return tasks;
            }
        case REMOVE_TASK_SUCCESS:
            {
                return tasks;
            }
        default:
            {
                return state;
            }
    }
}

export const task = (state = {}, {type, task}) => {
    switch (type) {
        case SAVE_TASK_SUCCESS:
            {
                return task;
            }
        default:
            {
                return state;
            }
    }
}