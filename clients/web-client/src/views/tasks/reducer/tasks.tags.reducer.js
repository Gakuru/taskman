import {GET_USERS_SUCCESS, SAVE_TASK_SUCCESS} from "../types/tasks.types";

export const users = (state = {}, {type, users}) => {
    switch (type) {
        case GET_USERS_SUCCESS:
            {
                return users;
            }
        default:
            {
                return state;
            }
    }
}

export const tags = (state = {}, {type, tags}) => {
    switch (type) {
        case SAVE_TASK_SUCCESS:
            {
                return tags
                    ? tags
                    : state;
            }
        default:
            {
                return state;
            }
    }
}