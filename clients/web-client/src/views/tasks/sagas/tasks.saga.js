import {put, call, takeLatest, all} from 'redux-saga/effects';

import {REQUEST_GET_TASKS, REQUEST_SAVE_TASK, REQUEST_REMOVE_TASK, REQUEST_GROUP_TASK} from '../types/tasks.types';
import {getTasksSuccess, saveTaskSuccess, removeTaskSuccess, groupTaskSuccess} from '../actions/tasks.actions';

import {fetchTasks, saveTask, removeTask, groupTask} from '../../../api/tasks';
import {isOAuthed} from '../../../utils/token';

function * _fetchTasks({url}) {
    try {
        const data = yield call(fetchTasks, url);
        if (isOAuthed(data.code)) 
            if (data.success) 
                yield put(getTasksSuccess(data));
            }
        catch (e) {
        console.log(e);
    }
}

function * _saveTask({task}) {
    try {
        const data = yield call(saveTask, task);
        if (isOAuthed(data.code)) 
            if (data.success) 
                yield put(saveTaskSuccess(data));
            }
        catch (e) {
        console.log(e);
    }
}

function * _removeTask({taskId}) {
    try {
        const data = yield call(removeTask, taskId);
        if (isOAuthed(data.code)) 
            if (data.success) 
                yield put(removeTaskSuccess(data));
            }
        catch (e) {
        console.log(e);
    }
}

function * _groupTask({group}) {
    try {
        const data = yield call(groupTask, group);
        if (isOAuthed(data.code)) 
            if (data.success) 
                yield put(groupTaskSuccess(data));
            }
        catch (e) {
        console.log(e);
    }
}

function * fetchTasksSaga() {
    yield takeLatest(REQUEST_GET_TASKS, _fetchTasks);
}

function * saveTaskSaga() {
    yield takeLatest(REQUEST_SAVE_TASK, _saveTask);
}

function * removeTaskSaga() {
    yield takeLatest(REQUEST_REMOVE_TASK, _removeTask);
}

function * groupTaskSaga() {
    yield takeLatest(REQUEST_GROUP_TASK, _groupTask);
}

export default function * tasksSaga() {
    yield all([fetchTasksSaga(), saveTaskSaga(), removeTaskSaga(), groupTaskSaga()]);
}