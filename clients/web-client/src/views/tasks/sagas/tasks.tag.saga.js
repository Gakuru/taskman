import {put, call, takeLatest, all} from 'redux-saga/effects';

import {REQUEST_GET_USERS, REQUEST_TAG_TASK} from '../types/tasks.types';
import {getUsersSuccess, tagTaskSuccess} from '../actions/tasks.actions';

import {tagTask, getUsers} from '../../../api/tasks';
import {isOAuthed} from '../../../utils/token';

function * _fetchUsers({param, url}) {
    try {
        const data = yield call(getUsers, {param, url});
        if (isOAuthed(data.code)) 
            if (data.success) 
                yield put(getUsersSuccess(data));
            }
        catch (e) {
        console.log(e);
    }
}

function * _tagTask({tag}) {
    try {
        const data = yield call(tagTask, tag);
        if (isOAuthed(data.code)) 
            if (data.success) 
                yield put(tagTaskSuccess(data));
            }
        catch (e) {
        console.log(e);
    }
}

function * fetchUsersSaga() {
    yield takeLatest(REQUEST_GET_USERS, _fetchUsers);
}

function * tagTaskSaga() {
    yield takeLatest(REQUEST_TAG_TASK, _tagTask);
}

export default function * tagTasksSaga() {
    yield all([fetchUsersSaga(), tagTaskSaga()]);
}