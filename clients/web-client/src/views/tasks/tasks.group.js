import React from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {Link} from 'react-router-dom';

import {
    Card,
    CardHeader,
    CardBody,
    Row,
    Col,
    Button
} from 'reactstrap';

import {requestFindGroups} from '../groups/actions/groups.actions';
import {requestGroupTask} from './actions/tasks.actions';

class GroupTask extends React.Component {

    constructor(props) {
        super(props);

        this.taskId = props.match.params.taskId;

        this.state = {
            isTyping: false,
            groups: []
        }

        this.getGroups = this
            .getGroups
            .bind(this);

        this.renderSuggestBox = this
            .renderSuggestBox
            .bind(this);

        this.renderSuggestions = this
            .renderSuggestions
            .bind(this);

        this.groupTask = this.groupTask = this
            .groupTask
            .bind(this);
    }

    componentDidUpdate(previousProps) {
        if (previousProps.groups !== this.props.groups) {
            this.setState({groups: this.props.groups.groups});
        }
    }

    getGroups = event => {
        let value = event.target.value;
        let isTyping = value.length > 0
            ? true
            : false;
        this
            .props
            .requestFindGroups(value);

        this.setState({isTyping});
    }

    groupTask = (taskId, groupId) => {
        this
            .props
            .requestGroupTask({taskId, groupId});
    }

    renderSuggestBox = () => {
        if (this.state.isTyping) {
            return (
                <div>
                    {this.renderSuggestions()}
                </div>
            );
        }
    }

    renderSuggestions = () => {
        if (this.state.groups) {
            const {groups} = this.state;
            if (groups) {
                return groups.map(group => {
                    return (
                        <div
                            key={group.id}
                            style={{
                            marginTop: 10,
                            padding: 5,
                            borderBottom: 'solid',
                            borderBottomColor: '#f1f1f1',
                            borderBottomWidth: 1
                        }}>
                            <Row>
                                <Col>
                                    <div
                                        style={{
                                        paddingTop: 12
                                    }}>
                                        {group.name}
                                    </div>
                                </Col>
                                <Col>
                                    <div className="float-right">
                                        <Button
                                            outline
                                            color="primary"
                                            onClick={() => this.groupTask(group.id, this.taskId)}>
                                            Add
                                        </Button>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    )
                });
            }
        }
    }

    render() {
        return (

            <div className="animated fadeIn">
                <Row>
                    <Col xs="12" md="12" lg="12">
                        <Card>
                            <CardHeader>
                                <i className="icon-people"/>
                                &nbsp; Group Task
                            </CardHeader>
                            <CardBody>
                                <div className="breadcrumb">
                                    <Link to={`/tasks`} className="btn btn-outline-primary">
                                        <i className="fa fa-arrow-left"/>
                                        &nbsp; Back
                                    </Link>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="tagusers">Select employees:</label>
                                    <input className="form-control" onChange={event => this.getGroups(event)}/> {this.renderSuggestBox()}
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

}

const mapStateToProps = (store) => {
    return {groups: store.groups}
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    requestFindGroups,
    requestGroupTask
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(GroupTask);