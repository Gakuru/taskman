import React from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {Link} from 'react-router-dom';

import {
    Card,
    CardHeader,
    CardBody,
    Table,
    Button,
    ButtonGroup,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Col,
    Row
} from 'reactstrap';

import {requestGetTasks, requestRemoveTask} from './actions/tasks.actions';

const object = {
    isEmpty: (obj) => {
        return (Object.keys(obj).length === 0 && obj.constructor === Object
            ? true
            : false);
    }
}

class Tasks extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            rSelected:'all',
            removing: false,
            modal: true,
            tasks:[]
        };

        this.renderTasks = this
            .renderTasks
            .bind(this);

        this.toggleModal = this
            .toggleModal
            .bind(this);

        this.renderModal = this
            .renderModal
            .bind(this);
        
        this.onRadioBtnClick = this.onRadioBtnClick.bind(this);
    }

    componentDidMount() {
        this
            .props
            .requestGetTasks();
    }

    componentDidUpdate(previousProps) {
        if (previousProps.tasks !== this.props.tasks) {
            this.setState({
                modal: !this.state.modal,
                removing: false
            });
            this.setState({ tasks: this.props.tasks.tasks });
        }
    }

    onRadioBtnClick(rSelected) {
        this.setState({ rSelected });
        const { tasks } = this.props.tasks;
        let filtered = [];
        if (rSelected.toLowerCase() === 'all')
            filtered = tasks.filter(tasks => { return tasks.status.toLowerCase() !== rSelected.toLowerCase() });
        else
            filtered = tasks.filter(tasks => { return tasks.status.toLowerCase() === rSelected.toLowerCase() });
        this.setState({ tasks: filtered });
    }

    toggleModal(task) {
        this.setState({
            modal: !this.state.modal,
            task: task
        });
    }

    removeTask() {
        this
            .props
            .requestRemoveTask(this.state.task.id);
        this.setState({removing: true});
    }

    renderModal() {
        if (this.state.task) {
            return (
                <div>
                    <Modal isOpen={this.state.modal} toggle={this.toggle}>
                        <ModalHeader toggle={this.toggleModal}>Remove Task</ModalHeader>
                        <ModalBody>
                            Remove task: {this.state.task.text}
                        </ModalBody>
                        <ModalFooter>
                            <Button outline color="primary" onClick={this.toggleModal}>Cancel</Button>
                            <Button
                                outline
                                color={this.state.removing
                                ? 'warning'
                                : 'danger'}
                                disabled={this.state.removing}
                                onClick={this
                                .removeTask
                                .bind(this)}>
                                {this.state.removing
                                    ? 'Removing...'
                                    : 'Remove'}
                            </Button>
                        </ModalFooter>
                    </Modal>
                </div>
            )
        }
    }

    renderTasks = () => {
        if (!object.isEmpty(this.state.tasks)) {
            const {tasks} = this.state;
            return (
                <Table hover striped responsive>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Creator</th>
                            <th>Task</th>
                            <th>Scope</th>
                            <th>Status</th>
                            <th>
                                <div className="float-right">Actions</div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {tasks.map((task, i) => {
                            return (
                                <tr key={task.id}>
                                    <td>{(++i)}</td>
                                    <td>{task.name}</td>
                                    <td>{task.text}</td>
                                    <td>{task.scope}</td>
                                    <td>{task.status}</td>
                                    <td>
                                        <div className="float-right">
                                            <Link
                                                to={`/tasks/${task.id}/tag`}
                                                className="btn btn-outline-info btn-sm"
                                                title="Tag others to this task">
                                                <i className="fa fa-tags"/>
                                            </Link>
                                            &nbsp;
                                            <Link
                                                to={`/tasks/${task.id}/group`}
                                                className="btn btn-outline-dark btn-sm"
                                                title="Add task to a group">
                                                <i className="icon-people"/>
                                            </Link>
                                            &nbsp;
                                            <Link
                                                className="btn btn-outline-primary btn-sm"
                                                title="Edit this task"
                                                to={{
                                                pathname: `/tasks/${task.id}/edit`,
                                                state: {
                                                    task: task
                                                }
                                            }}>
                                                <i className="fa fa-edit"></i>
                                            </Link>
                                            &nbsp;
                                            <Button
                                                outline
                                                color="danger"
                                                size="sm"
                                                title="Delete this task"
                                                onClick={() => this.toggleModal(task)}>
                                                <i className="fa fa-trash"/>
                                            </Button>
                                        </div>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </Table>
            );
        }
    }

    render() {
        return (

            <div className="animated fadeIn">
                {this.renderModal()}
                <Card>
                    <CardHeader>
                        <Row className="align-items-center">
                            <Col>
                                <div className="float-left">
                                <Link to={`/tasks/create`} className="btn btn-outline-primary">
                                <i className="icon-plus"></i>
                                &nbsp; New Task
                            </Link>
                                </div>
                            </Col>
                            <Col>
                                <div className="float-right">
                                    <ButtonGroup>
                                        <Button color="primary" onClick={() => this.onRadioBtnClick('all')} active={this.state.rSelected === 'all'}>All</Button>
                                        <Button color="warning" onClick={() => this.onRadioBtnClick('pending')} active={this.state.rSelected === 'pending'}>Pending</Button>
                                        <Button color="success" onClick={() => this.onRadioBtnClick('complete')} active={this.state.rSelected === 'complete'}>Complete</Button>
                                    </ButtonGroup>
                                </div>
                            </Col>
                        </Row>
                    </CardHeader>
                </Card>

                <Row>
                    <Col xs="12" md="12" lg="12">
                        <Card>
                            <CardHeader>
                                <i className="icon-notebook"></i>
                                My Tasks
                            </CardHeader>
                            <CardBody>
                                {this.renderTasks()}
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = (store) => {
    return {tasks: store.tasks}
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    requestGetTasks,
    requestRemoveTask
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Tasks);