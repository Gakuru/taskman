import React from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {Link} from 'react-router-dom';

import {
    Card,
    CardHeader,
    CardBody,
    Row,
    Col,
    Button
} from 'reactstrap';

import {requestGetUsers, requestTagTask} from './actions/tasks.actions';

class TagTask extends React.Component {

    constructor(props) {
        super(props);

        this.taskId = props.match.params.taskId;

        this.state = {
            isTyping: false,
            users: []
        }

        this.getUsers = this
            .getUsers
            .bind(this);

        this.renderSuggestBox = this
            .renderSuggestBox
            .bind(this);

        this.renderSuggestions = this
            .renderSuggestions
            .bind(this);

        this.tagUser = this.tagUser = this
            .tagUser
            .bind(this);
    }

    componentDidUpdate(previousProps) {
        if (previousProps.users !== this.props.users) {
            this.setState({users: this.props.users.users});
        }
    }

    getUsers = event => {
        let value = event.target.value;
        let isTyping = value.length > 0
            ? true
            : false;
        this
            .props
            .requestGetUsers(value);

        this.setState({isTyping});
    }

    tagUser = (userId, taskId) => {
        this
            .props
            .requestTagTask({userId, taskId});
    }

    renderSuggestBox = () => {
        if (this.state.isTyping) {
            return (
                <div>
                    {this.renderSuggestions()}
                </div>
            );
        }
    }

    renderSuggestions = () => {
        if (this.state.users) {
            const {users} = this.state;
            if (users) {
                return users.map(user => {
                    return (
                        <div
                            key={user.id}
                            style={{
                            marginTop: 10,
                            padding: 5,
                            borderBottom: 'solid',
                            borderBottomColor: '#f1f1f1',
                            borderBottomWidth: 1
                        }}>
                            <Row>
                                <Col>
                                    <div>
                                        <img
                                            className="rounded-circle"
                                            src={user.image_url}
                                            alt={user.last_name}
                                            width={48}
                                            height={48}/>
                                    </div>
                                </Col>
                                <Col>
                                    <div
                                        style={{
                                        paddingTop: 12
                                    }}>
                                        {user.name}
                                    </div>
                                </Col>
                                <Col>
                                    <div className="float-right">
                                        <Button
                                            outline
                                            color="primary"
                                            onClick={() => this.tagUser(user.id, this.taskId)}>
                                            Add
                                        </Button>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    )
                });
            }
        }
    }

    render() {
        return (

            <div className="animated fadeIn">
                <Row>
                    <Col xs="12" md="12" lg="12">
                        <Card>
                            <CardHeader>
                                <i className="fa fa-tags"/>
                                &nbsp; Tag Task
                            </CardHeader>
                            <CardBody>
                                <div className="breadcrumb">
                                    <Link to={`/tasks`} className="btn btn-outline-primary">
                                        <i className="fa fa-arrow-left"/>
                                        &nbsp; Back
                                    </Link>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="tagusers">Select employees to tag:</label>
                                    <input className="form-control" onChange={event => this.getUsers(event)}/> {this.renderSuggestBox()}
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

}

const mapStateToProps = (store) => {
    return {users: store.users}
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    requestGetUsers,
    requestTagTask
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TagTask);